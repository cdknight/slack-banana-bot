require 'rubygems'
require 'bundler/setup'
require 'slack-ruby-bot'
require 'mysql2'
require 'json'
require 'pp'

$config = {}

File.open("config.json") do |file|
  $config = JSON.parse(file.read)
end

$config["start_user"] = $config["start_user"].upcase

ENV["SLACK_API_TOKEN"] = $config["slack_token"]

$client = Mysql2::Client.new(
  :host => $config['mysql_host'],
  :username => $config["mysql_user"],
  :password => $config["mysql_password"],
  :port => $config["mysql_port"],
  :database => $config["mysql_dbname"]
)

class GiveawayManager
  def self.start
    $client.query "UPDATE status SET running=1"
  end
  def self.status
    status = false 
    $client.query("SELECT running FROM status").each do |row|
      if row['running'] == 1
        status = true
      end
    end

    status
  end
  def self.end
    $client.query "UPDATE status SET running=0"
  end
end

class Registrant

  attr_reader :address, :name, :user, :winner
  attr_writer :winner

  def initialize(name, address, user)
    @name = name
    @address = address
    @user = user
    @winner = 0
  end

  def self.from_db(user)
    # Fetch from the database

    user = $client.escape user
    name = ""
    address = ""
    winner = 0

    query = "SELECT * FROM entrants WHERE user='#{user}'"

    result = $client.query(query)
    # Return nil if no entry
    if result.count == 0
      return nil
    end


    result.each do |row|
      # Will work since there is one row

      puts row

      name = row['name']
      address = row['address']
      winner = row['winner']
    end

    Registrant.new name, address, user

  end

  def address=(address)
    @address = $client.escape address
  end

  def user=(user)
    @user = $client.escape user
  end

  def name=(name)
    @name = $client.escape name
  end

  def insert
    # Insert the registrant into the table
    query = "INSERT INTO entrants(name, address, user, winner) VALUES ('#{@name}', '#{@address}', '#{@user}', 0)"
    $client.query query
  end

  def save
    # There is only one row for each user
    query = "UPDATE entrants SET name='#{@name}', address='#{@address}', winner='#{@winner}' WHERE user='#{@user}'";
    $client.query query
  end

  def delete
    query = "DELETE FROM entrants WHERE user='#{user}'"
    $client.query query
  end
end

class BananaBot < SlackRubyBot::Bot
  match /enter name (?<name>.*) address (?<address>.*)$/ do |client, data, match|

    if data[:channel][0] == "D"

      if not GiveawayManager.status
        client.say(text: "There is no active giveaway!", channel: data.channel)
        return
      end


      # It's a DM. We can continue.
      address = match[:address]
      name = match[:name]
      user = data.user

        # Check if already exists
        if Registrant.from_db(user)

          client.say(text: "You've already entered into the giveaway!", channel: data.channel)

          return
        end


        entry = Registrant.new name, address, user
        entry.insert

        response = "Enrolled #{name} with adddress #{address} in the monthly banana giveaway"
        client.say(text: response, channel: data.channel)


    else
      client.say(text: "Don't enter a giveaway here! :point_right: You can enter a banana giveaway by DM'ing me.", channel: data.channel)
    end

  end

  command 'delete' do |client, data, match|


    if data[:channel][0] == "D"

      if not GiveawayManager.status
        client.say(text: "There is no active giveaway!", channel: data.channel)
        return
      end


      entry = Registrant.from_db data.user

      if entry == nil
        client.say(text: "You have not entered into the giveaway!", channel: data.channel)
      else
        entry.delete
        client.say(text: "Entry deleted succesfully! :thumbsup:", channel: data.channel)
      end

    end

  end

  match(/edit address (?<address>.*)$/) do |client, data, match|

    if data[:channel][0] != "D"
      return
    end

    if not GiveawayManager.status
      client.say(text: "There is no active giveaway!", channel: data.channel)
      return
    end


    address = match[:address]
    user = data.user

    entry = Registrant.from_db data.user
    if entry == nil
      client.say(text: "You have not entered into the giveaway!", channel: data.channel)
    else
      entry.address = address
      entry.save
      client.say(text: "Address updated succesfully! :thumbsup:", channel: data.channel)
    end

  end

  match(/edit name (?<name>.*)$/) do |client, data, match|

    if data[:channel][0] != "D"
      return
    end

    if not GiveawayManager.status
      client.say(text: "There is no active giveaway!", channel: data.channel)
      return
    end


    name = match[:name]
    user = data.user

    entry = Registrant.from_db data.user
    if entry == nil
      client.say(text: "You have not entered into the giveaway!", channel: data.channel)
    else
      entry.name = name
      entry.save
      client.say(text: "Name updated succesfully! :thumbsup:", channel: data.channel)
    end

  end


  command "start" do |client, data, match|
    # The user must be the start_user
    if data.user == $config["start_user"]

      if GiveawayManager.status
        client.say(text: "You have already started the giveaway!", channel: data.channel)
      else
        GiveawayManager.start
        client.say(text: "Giveaway started!", channel: data.channel)
      end

    else
      client.say(text: "You do not have permissions to start a giveaway!", channel: data.channel)
    end
  end

  command "end" do |client, data, match|
    # The user must be the start_user
    if data.user == $config["start_user"]

      if GiveawayManager.status
      # Randomize and choose winner here

        users_r = $client.query "SELECT user FROM entrants"

        # For randomizing
        users = []
        users_r.each do |row| users << row['user'] end

        chosen_user = users.sample
        entry = Registrant.from_db chosen_user

        client.say(text: "... And the winners have been chosen!", channel: data.channel)

        # Open IM
        web_client = ::Slack::Web::Client.new(token: ENV['SLACK_API_TOKEN'])
        im = web_client.im_open(user: $config["start_user"])

        client.say(text: "The winner is *#{entry.name}* and their address is *#{entry.address}*! (User <@#{entry.user}>)", channel: im['channel']['id'])

      # Clear up the registrant table
        $client.query "DELETE FROM entrants"

        # End the giveaway
        GiveawayManager.end





      else
        client.say(text: "You have already ended the giveaway!", channel: data.channel)

      end

    else
      client.say(text: "You do not have permissions to end a giveaway!", channel: data.channel)
    end
  end




end

BananaBot.run
