create table entrants (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(200), address VARCHAR(500), user VARCHAR(20) unique, winner BOOL);
create table status (running INT);
insert into status values(0);
